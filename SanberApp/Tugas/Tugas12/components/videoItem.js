import React, {Component} from 'react';
import { Platform, View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItem extends Component {
    render() {
        let video = this.props.video;
        return (
            <View style={styles.container}>
                <Image source={{url:video.snippet.thumbnails.medium.url}} style={{height:200}}/>
                <View style={styles.descContainer}> 
                    <Image source={{url:'https://randomuser.me/api/portraits/men/0.jpg'}} style={{width:50, height: 50}}/>
                </View>    
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    descContainer:{
        flexDirection: 'row',
        paddingTop: 15
    }
});