import React, {Component} from 'react';
import { Platform, View, Text, StyleSheet, Image, TouchableOpacity, TextInput, Button} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends React.Component {
    render() {
        return (
        <View style={styles.container}>
            <View style={styles.navBar}>
                    <Image source={require('./images/logo1.png')} style={{width:250, height:250}}/>
            </View>

            <View style={styles.body}>
                <View style={styles.bodyTitle}>
                        <Text style={styles.bodyTextTitle}>SIGN IN</Text>
                </View>
                <View style={styles.bodyMain}>
                    <TextInput 
                        style={styles.bodyTextInput}
                        placeholder="Username"
                    />
                    <TextInput 
                        secureTextEntry
                        style={styles.bodyTextInput}
                        placeholder="Password"
                    />
                    <TouchableOpacity style={{ flexDirection: 'row', alignSelf: 'flex-end', marginRight: '10%'}}>
                        <Text style={styles.linkTextBoldPass}>Forgot Password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={styles.linkText}>Not Registered?</Text>
                        <TouchableOpacity>
                            <Text style={styles.linkTextBold}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            <View style={styles.tabBar}>
                <Text style={styles.tabText}>SanberCode 1.0</Text>
            </View>

        </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 300,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  body: {
    flex: 1,
    flexDirection: 'column',
    marginBottom:20,
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 20
  },
  bodyTitle:{
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  bodyTextTitle:{
    fontSize: 24,
    color: '#CF7500',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: 4
  },
  bodyMain: {
    flex: 1,
    height:50,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginBottom:20
  },
  bodyTextInput:{
    height: 30, 
    width: '70%',
    borderColor: '#C4C4C4', 
    backgroundColor: '#C4C4C4',
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: 10,
    marginTop:20
  },
  linkText:{
    color: '#C4C4C4',
    margin: 10
  },
  linkTextBold:{
    color: '#C4C4C4',
    fontWeight: 'bold'
  },
  linkTextBoldPass:{
    color: '#C4C4C4',
    fontWeight: 'bold',
    textAlign: 'right',
    margin: 10
  },
  buttonStyle:{
    width: '30%',
    height: 30,
    backgroundColor: '#F0A500',
    borderRadius: 10, 
    textAlign: 'center',
    alignItems: 'center'
  },
  buttonText:{
    fontSize: 14,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    padding: 5
  },
  tabBar: {
    backgroundColor: 'white',
    height: 40,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  tabText: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
})
