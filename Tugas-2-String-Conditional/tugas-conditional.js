/*--------------------- if else -----------------------*/
// Petunjuk : Kita akan memasuki dunia game werewolf. Pada saat akan bermain kamu diminta memasukkan nama dan peran. 
// Untuk memulai game pemain harus memasukkan variable nama dan peran. Jika pemain tidak memasukkan nama maka game 
// akan mengeluarkan warning “Nama harus diisi!“. Jika pemain memasukkan nama tapi tidak memasukkan peran maka game 
// akan mengeluarkan warning “Pilih Peranmu untuk memulai game“. Terdapat tiga peran yaitu penyihir, guard, dan werewolf.
// Tugas kamu adalah membuat program untuk mengecek input dari pemain dan hasil response dari game sesuai input yang dikirimkan.

 
//Petunjuk:
/*  Nama dan peran diisi manual dan bisa diisi apa saja
    Nama tidak perlu dicek persis sesuai dengan input/output
    Buat kondisi if-else untuk masing-masing peran
*/


//Input
var nama    = 'Bambang';
var peran   = 'Warewolf';

console.log("---------- If Else ---------------");

//Program

if (nama === '' && peran === '') {
    console.log('Nama harus diisi!');
} else if ( nama  && peran === '') {
    console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
} else if ( nama && peran) {
    console.log('Selamat datang di Dunia Warewolf, ' + nama);
    if (peran.toLowerCase() === 'penyihir') {
        console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi warewolf!');
    } else if (peran.toLowerCase() === 'guard') {
        console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi taman dari serangan warewolf.');
    } else if (peran.toLowerCase() === 'warewolf'){
        console.log('Halo Warewolf ' + nama + ', kamu akan memakan mangsa setiap malam!');
    }
}

console.log("\n");
/*-----------------------------------------------------------*/





/*-------------------------- Switch -----------------------------*/
//Kamu akan diberikan sebuah tanggal dalam tiga variabel, yaitu hari, bulan, dan tahun. 
//Disini kamu diminta untuk membuat format tanggal. Misal tanggal yang diberikan adalah hari 1, bulan 5, 
//dan tahun 1945. Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.

//Gunakan switch case untuk kasus ini!

//Input 
var hari    = 8;
var bulan   = 8;
var tahun   = 2018;


console.log("---------Switch------------");


//Program 

if (hari >= 1 && hari <= 31 && bulan >= 1 && bulan <= 12  && tahun >= 1900 && tahun <= 2200){
    
    switch(bulan){
        case 1 :{
            console.log(hari + " Januari " + tahun);
            break;
        } 
        case 2 :{
            console.log(hari + " Februari " + tahun);
            break;
        } 
        case 3 :{
            console.log(hari + " Maret " + tahun);
            break;
        } 
        case 4 :{
            console.log(hari + " April " + tahun);
            break;
        } 
        case 5 :{
            console.log(hari + " Mei " + tahun);
            break;
        } 
        case 6 :{
            console.log(hari + " Juni " + tahun);
            break;
        } 
        case 7 :{
            console.log(hari + " Juli " + tahun);
            break;
        } 
        case 8 :{
            console.log(hari + " Agustus " + tahun);
            break;
        } 
        case 9 :{
            console.log(hari + " September " + tahun);
            break;
        } 
        case 10 :{
            console.log(hari + " Oktober " + tahun);
            break;
        } 
        case 11 :{
            console.log(hari + " November " + tahun);
            break;
        } 
        case 12 :{
            console.log(hari + " Desember " + tahun);
            break;
        } 
    }
}