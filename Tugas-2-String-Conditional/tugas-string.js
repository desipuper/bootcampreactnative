/* -----------Soal 1 (Membuat Kalimat)-----------------*/

var word    = 'JavaScript';
var second  = 'is';
var third   = 'awesome';
var fourth  = 'and';
var fifth   = 'I';
var sixth   = 'love';
var seventh = 'it!';

// Buatlah agar kata-kata di ats menjadi satu kalimat.
// Output : JavaScript is awesome and I love it!

//JAWABAN
console.log("----------Jawaban 1---------");

//print dengan concat
var output = word.concat(' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh);
console.log(output);

console.log("\n");

/*----------------------------------------*/






/*-------------Soal 2 Menguraikan Kalimat (Akses karakter dalam string)---------------*/

var sentence = "I am going to be React Native Developer";

var firstWord   = sentence[0];
var secondWord  = sentence[2] + sentence[3];
var thirdWord   = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord  = sentence[11] + sentence[12];
var fifthWord   = sentence[14] + sentence[15];
var sixthWord   = sentence[17] + sentence[18] + sentence[19] + sentence[20] +sentence[21];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord  = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] +sentence[36] + sentence[37] + sentence[38];


//Buat menjadi output berikut :
/*
    First word : I
    Second word : am 
    Thrid word : going 
    Fourt word : to 
    Fifth word : be 
    Sixth word : React
    Seventh word : Native
    Wigth word : Developer
*/

//JAWABAN
console.log("----------Jawaban 2---------");

console.log('First word: ' + firstWord);
console.log('Second word: ' + secondWord);
console.log('Third word: ' + thirdWord);
console.log('Fourth word: ' + fourthWord);
console.log('Fifth word: ' + fifthWord);
console.log('Sixth word: ' + sixthWord);
console.log('Seventh word: ' + seventhWord);
console.log('Eighth word: ' + eighthWord);

console.log("\n");
/*-----------------------------------------------------------*/






/*----------------- Soal 3 Mengurai kalimat (substring) ----------------*/
var sentence2   = 'wow JavaScript is so cool';

var firstWord2  = sentence2.substring(0,3);
var secondWord2 = sentence2.substring(4,14);
var thirdWord2  = sentence2.substring(15,17);
var fourthWord2 = sentence2.substring(18,20);
var fifthWord2  = sentence2.substring(21,25);

//Uraikanlah kalimat sentence2 di atas menjadi kata-kata penyusunannya. 
//Output:
/*
First Word: wow 
Second Word: JavaScript 
Third Word: is 
Fourth Word: so 
Fifth Word: cool
*/

//JAWABAN
console.log("----------Jawaban 3---------");

console.log('First word: ' + firstWord2);
console.log('Second word: ' + secondWord2);
console.log('Third word: ' + thirdWord2);
console.log('Fourth word: ' + fourthWord2);
console.log('Fifth word: ' + fifthWord2);

console.log("\n");
/*-----------------------------------------------------------*/





/*------------------Soal 4 Mengurai kalimat dan menentukan panjang string-------------------*/
var sentence3   = 'wow JavaScript is so cool';

var firstWord3  = sentence3.substring(0,3);
var secondWord3 = sentence3.substring(4,14);
var thirdWord3  = sentence3.substring(15,17);
var fourthWord3 = sentence3.substring(18,20);
var fifthWord3  = sentence3.substring(21,25);


//Output
/*
First Word: wow, with length: 3 
Second Word: JavaScript, with length: 10 
Third Word: is, with length: 2 
Fourth Word: so, with length: 2 
Fifth Word: cool, with length: 4
*/

//JAWABAN
console.log("----------Jawaban 4---------");

console.log('First word: ' + firstWord3 + ', with length: ' + firstWord3.length);
console.log('Second word: ' + secondWord3 + ', with length: ' + secondWord3.length);
console.log('Third word: ' + thirdWord3 + ', with length: ' + thirdWord3.length);
console.log('Fourth word: ' + fourthWord3 + ', with length: ' + fourthWord3.length);
console.log('Fifth word: ' + fifthWord3 + ', with length: ' + fifthWord3.length);

console.log("\n");
/*-----------------------------------------------------------*/