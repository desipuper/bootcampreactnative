/*------------------Soal 1 Looping While---------------------*/
// Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan 
// menggunakan syntax while. Untuk membuat tantangan ini lebih menarik, kamu juga 
// diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur.
// Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.”

//Output:
/*
LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding
LOOPING KEDUA
20 - I will become a mobile developer
18 - I will become a mobile developer                                                                              
16 - I will become a mobile developer
14 - I will become a mobile developer
12 - I will become a mobile developer
10 - I will become a mobile developer
8 - I will become a mobile developer
6 - I will become a mobile developer
4 - I will become a mobile developer
2 - I will become a mobile developer
*/

//PROGRAM
console.log("---------------- JAWABAN SOAL 1 -------------------");

var no      = 0;
var batas   = 20;

// Looping Pertama (Maju)
console.log("LOOPING PERTAMA");
while( no < batas){
    no+=2;
    console.log(no + ' - I love coding');
}

//Looping Kedua (Mundur)
console.log("LOOPING KEDUA");
while( no > 0){
    console.log(no + ' - I will become a mobile developer');
    no-=2;
}
console.log('\n');

/*---------------------------------------------------------------------*/





/*------------------- SOAL 2 Looping menggunakan For -------------------------*/

//Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for. 
//Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:

//SYARAT:
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.

//OUTPUT 
/*
1 - Santai
2 - Berkualitas
3 - I Love Coding 
4 - Berkualitas
5 - Santai
6 - Berkualitas
7 - Santai
8 - Berkualitas
9 - I Love Coding
10 - Berkualitas
11 - Santai
12 - Berkualitas
13 - Santai
14 - Berkualitas
15 - I Love Coding
16 - Berkualitas
17 - Santai
18 - Berkualitas
19 - Santai
20 - Berkualitas
*/

//PROGRAM
console.log("---------------- JAWABAN SOAL 2 -------------------");

var batas2 = 20;

for(var i = 1; i <= batas2; i++){
    if (i % 2 == 0){
        console.log(i + ' - Berkualitas');
    } else {
        if (i % 3 == 0) {
            console.log(i + ' - I Love Coding');
        } else {
            console.log(i + ' - Santai');
        }
    }
}

console.log('\n');

/*--------------------------------------------------------------------*/





/*-------------------- Soal 3 Membuat persegi panjang # ----------------------*/

//Kamu diminta untuk menampilkan persegi dengan dimensi 8×4 dengan tanda pagar (#) dengan perulangan 
//atau looping. Looping boleh menggunakan syntax apa pun (while, for, do while).

//Output:
/*
########
########
########
######## 
*/


//PROGRAM
console.log("---------------- JAWABAN SOAL 3 -------------------");

var x       = 0;
var lebar   = 4;

var y       = 0;
var panjang = 8;

var cetak ='';


do{
    while (y < panjang) {
        cetak = cetak.concat('#');
        y++;
    }
    console.log(cetak);
    x++;
} 
while( x < lebar );


console.log('\n');

/*--------------------------------------------------------------------*/






/*------------------ Soal 4 Membuat Tangga ------------------------*/

// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi 
// tinggi 7 dan alas 7. Looping boleh menggunakan syntax apa pun (while, for, do while).

//Output:
/*
#
##
###
####
#####
######
#######
*/


//PROGRAM
console.log("---------------- JAWABAN SOAL 4 -------------------");

var tinggi  = 7;
var alas    = 7;
var n       = 0;

var cetak2  = '';

for(var m = 0; m < tinggi; m++){
    while(n < alas){
        if (m == n) {
            cetak2 = cetak2.concat('#');
            break;
        } else if(m < n){
            cetak2 = cetak2.concat('#');
        } 
        n++;
    }
    console.log(cetak2);
}

console.log('\n');

/*--------------------------------------------------------------------*/






/*------------------------- Soal 5 Membuat Papan Catur -------------------*/
// Buatlah suatu looping untuk menghasilkan sebuah papan catur dengan ukuran 8 x 8 . Papan berwarna 
// hitam memakai tanda pagar (#) sedangkan papan putih menggunakan spasi. Looping boleh menggunakan 
// syntax apa pun (while, for, do while).

//Output:
/*
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
*/

//PROGRAM
console.log("---------------- JAWABAN SOAL 5 -------------------");

var ukuranPanjang   = 8; 
var ukuranLebar     = 8;

for(var p = 1; p <= ukuranPanjang ; p++){
    var cetak3 = '';
    for(var l = 1; l <= ukuranLebar ; l++) {
        if(p % 2 == 0){
            if(l % 2 != 0){
                cetak3   = cetak3.concat('#');
            } else {
                cetak3   = cetak3.concat(' ');
            }
        } else {
            if(l % 2 == 0){
                cetak3   = cetak3.concat('#');
            } else {
                cetak3   = cetak3.concat(' ');
            }
        }
    }
    console.log(cetak3);
}

console.log('\n');

/*--------------------------------------------------------------------*/
