/*---------------------- Soal 1 Function Teriak -------------------------*/
// Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai “Halo Sanbers!” 
// yang kemudian dapat ditampilkan di console.

// OUTPUT:
/* "Halo Sanbers!" */

//FUNCTION
function teriak(){
    return "Halo Sanbers!";
}

console.log('------ 1. Functions teriak() --------------');

console.log(teriak());

console.log('\n');
/*-------------------------------------------------------*/





/*---------------------- Soal 2 Function Kalikan-------------------------*/
// Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter 
// yang di kirim.

// OUTPUT : 
/* 48 */

//FUNCTION
function kalikan(bilangan1, bilangan2){
    return bilangan1 * bilangan2;
}


var num1 = 12;
var num2 = 4;

console.log('------ 2. Functions kalikan() --------------');

var kalikan     = kalikan(num1, num2);
console.log(kalikan);

console.log('\n');
/*-------------------------------------------------------*/




/*---------------------- Soal 3 Function Introduce -------------------------*/
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah 
// kalimat perkenalan seperti berikut: 

// OUTPUT:
/* “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!” */


//FUNCTION
function introduce(name, age, address, hobby){

    var result = 'Name saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!';
    
    return result;
}



var name    = "Agus";
var age     = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby   = "Gaming";
 
console.log('------ 1. Functions introduce() --------------');

var perkenalan  = introduce(name, age, address, hobby);
console.log(perkenalan);

console.log('\n');
/*-------------------------------------------------------*/
