/*------------------------ Soal 1 Range -------------------------*/
// Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. 
// Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama 
// hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua 
// maka angka-angka tersusun secara menurun (descending).

//struktur fungsinya seperti berikut range(startNum, finishNum) {}
//Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1

// OUTPUT
/* range(1, 10) --> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
   range(1) --> -1
   range(11,18) --> [11, 12, 13, 14, 15, 16, 17, 18]
   range(54, 50) --> [54, 53, 52, 51, 50]
   range() --> -1 
*/


//FUNCTION
function range(startNum, finishNum){

    if(!startNum || !finishNum){
        var resultArr;
        resultArr = -1;
    } else if(startNum > finishNum){
        var resultArr = [];
        for(var i = startNum; i >= finishNum ; i--){
            resultArr.push(i);
        }
    } else if(startNum < finishNum){
        var resultArr = [];
        for(var j = startNum; j <= finishNum ; j++){
            resultArr.push(j);
        }
    }

    return resultArr;
}


console.log('--------1. RANGE -----------');

console.log(range(1, 10)); 
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range()); 

console.log('\n');

/*-----------------------------------------------------*/







/*----------------------- Soal 2 Range with step ---------------------*/
// Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range 
// di soal sebelumnya namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan 
// selisih atau beda dari setiap angka pada array. Jika parameter pertama lebih besar dibandingkan parameter 
// kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.

// struktur fungsinya seperti berikut rangeWithStep(startNum, finishNum, step) {}

//OUTPUT
/* rangeWithStep(1, 10, 2) --->  [1, 3, 5, 7, 9]
   rangeWithStep(11, 23, 3) ---> [11, 14, 17, 20, 23]
   rangeWithStep(5, 2, 1) ---> [5, 4, 3, 2]
   rangeWithStep(29, 2, 4) ---> [29, 25, 21, 17, 13, 9, 5] 
*/

// FUNCTION

function rangeWithStep(startNum, finishNum, step) {

    if(!startNum || !finishNum){
        var resultArr;
        resultArr = -1;
    } else if(startNum > finishNum){
        var resultArr = [];
        while(startNum >= finishNum ){
            resultArr.push(startNum);
            startNum -= step;
        }
    } else if(startNum < finishNum){
        var resultArr = [];
        while(startNum <= finishNum){
            resultArr.push(startNum);
            startNum += step;
        }
    }

    return resultArr;

}


console.log('-------- 2. RANGE WITH STEP -----------');

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log('\n');
/*-----------------------------------------------------*/





/*----------------------- Soal 3 Sum of range ---------------------*/
// Kali ini kamu akan menjumlahkan sebuah range (Deret) yang diperoleh dari function range di soal-soal 
// sebelumnya. Kamu boleh menggunakan function range dan rangeWithStep pada soal sebelumnya untuk menjalankan 
// soal ini.

// Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, 
// angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka. 
// contohnya sum(1,10,1) akan menghasilkan nilai 55.

//ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1.

//OUTPUT
/*  sum(1,10) --> 55
    sum(5, 50, 2) --> 621
    sum(15,10) --> 75
    sum(20, 10, 2) --> 90
    sum(1)--> 1
    sum() --> 0 
*/

//FUNCTION

function sum(startNum, finishNum, step){

    var array = [];
    var i;
    var total = 0;

    if (startNum && finishNum || step){
        if (step){
            array   = rangeWithStep(startNum, finishNum, step);
            for(i = 0; i < array.length; i++) {
                total   += array[i];
            }
        } else {
            array   = range(startNum, finishNum);
            for(i = 0; i < array.length; i++) {
                total   += array[i];
            }
        }
    } else if (!startNum && !finishNum && !step){
        total = 0;
    } else{
        total = startNum;
    }
    return total;
}

console.log('-------- 3. SUM OF RANGE -----------');

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log('\n');
/*-----------------------------------------------------*/






/*----------------------- Soal 4 Array multidimensi ---------------------*/
// Sering kali data yang diterima dari database adalah array yang multidimensi (array di dalam array).
// Sebagai developer, tugas kita adalah mengolah data tersebut agar dapat menampilkan informasi yang diinginkan.

// Buatlah sebuah fungsi dengan nama dataHandling dengan sebuah parameter untuk menerima argumen. 
// Argumen yang akan diterima adalah sebuah array yang berisi beberapa array sejumlah n. 

//contoh input
/*
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
*/

// Tugas kamu adalah mengimplementasikan fungsi dataHandling() agar dapat menampilkan data-data 
// pada dari argumen seperti di bawah ini:
/*
Nomor ID:  0001
Nama Lengkap:  Roman Alamsyah
TTL:  Bandar Lampung 21/05/1989
Hobi:  Membaca
 
Nomor ID:  0002
Nama Lengkap:  Dika Sembiring
TTL:  Medan 10/10/1992
Hobi:  Bermain Gitar
 
Nomor ID:  0003
Nama Lengkap:  Winona
TTL:  Ambon 25/12/1965
Hobi:  Memasak
 
Nomor ID:  0004
Nama Lengkap:  Bintang Senjaya
TTL:  Martapura 6/4/1970
Hobi:  Berkebun 
*/

//FUNCTION 

function dataHandling(data) {
    
    var i, j ;
    var parameter = ["Nama ID" , "Nama Lengkap", "TTL",  ,"Hobi"];
    
    for(i = 0 ; i < data.length ; i++){
        for(j = 0 ; j < data[i].length; j++){
            if(j == 2 || j == 3){ 
                if(j == 3) {
                    console.log(parameter[j-1] + ' : ' + data[i][j-1] + ', ' + data[i][j]);
                }
            } else {
                console.log(parameter[j] +' : '+ data[i][j]);
            }
        } 
        console.log('\n');
    }
}

console.log('-------- 4. ARRAY MULTIDIMENSI -----------');

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);

console.log('\n');
/*-----------------------------------------------------*/





/*----------------------- Soal 5 Balik Kata ---------------------*/
// Kamu telah mempelajari beberapa method yang dimiliki oleh String dan Array. String sebetulnya adalah 
// sebuah array karena kita dapat mengakses karakter karakter pada sebuah string layaknya mengakses elemen 
// pada array.

// Buatlah sebuah function balikKata() yang menerima sebuah parameter berupa string dan mengembalikan 
// kebalikan dari string tersebut.

// Dilarang menggunakan sintaks .split , .join , .reverse() , hanya gunakan looping!

//OUTPUT
/*  balikKata("Kasur Rusak") --> kasuR rusaK
    balikKata("SanberCode") --> edoCrebnaS
    balikKata("Haji Ijah") --> hajI ijaH
    balikKata("racecar") --> racecar
    balikKata("I am Sanbers") --> srebnaS ma I 
*/

//FUNCTION

function balikKata(kata){
    var i, j;
    var balik = '';

    for(i = kata.length -1; i >= 0 ; i--){
        balik    += kata[i];
    }

    return balik;
}


console.log('-------- 5. BALIK KATA -----------');

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode")); 
console.log(balikKata("Haji Ijah")); 
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log('\n');
/*-----------------------------------------------------*/





/*----------------------- Soal 6 Metode Array ---------------------*/
// Array pada JavaScript memiliki sekumpulan built-in function yang digunakan untuk mempermudah developer 
// untuk mengolah data di dalamnya. Beberapa fungsi yang sering digunakan antara lain join, split, slice, 
// splice, dan sort. Kerjakanlah tantangan ini untuk memperkuat pengertian kamu tentang built-in function tersebut.

//PETUNJUK
/* 
1. Buatlah sebuah function dengan nama dataHandling2 yang akan menerima input array seperti di atas.
2. Gunakan fungsi splice untuk memodifikasi variabel tersebut agar menjadi seperti array dibawah. Lalu console.log array yang baru seperti di bawah.
["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
3. Berdasarkan elemen yang berisikan tanggal/bulan/tahun (elemen ke-4), ambil angka bulan dan console.log nama bulan sesuai dengan angka tersebut.
    a. Gunakan split untuk memisahkan antara tanggal, bulan, dan tahun.
    b. Format tanggal pada data adalah dd-mm-YYYY
    c. Misal angka bulan 01, tuliskan “Januari”
    d. Gunakan switch case untuk menuliskan bulan di atas.
4. Pada array hasil split dari tanggal/bulan/tahun, lakukan sorting secara descending dan console.log array yang sudah di-sort.
5. Masih pada array hasil split dari elemen tanggal/bulan/tahun, gabungkan semua elemen menggunakan join dan pisahkan dengan karakter strip (-) lalu console.log hasilnya.
6. Nama (elemen ke-2), harus dibatasi sebanyak 15 karakter saja. Gunakan slice untuk menghapus kelebihan karakter dan console.log nama yang sudah di-slice, sebelum di-slice pastikan Nama (elemen ke-2) sudah dalam bentuk String agar bisa di-slice.
*/

//OUTPUT
/*
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
*/

//FUNCTION

function dataHandling2(data){

    var detail              = data;

    var detail1             = detail[1] + 'Elsharawy';
    var detail2             = 'Provinsi ' + detail[2];
    var detail3             = "Pria";
    var detail5             = "SMA Internasional Metro";

    // menggunakan fungsi splice
    detail.splice(1,1, detail1);
    detail.splice(2,1, detail2);
    detail.splice(4,1, detail3, detail5);
    
    //print hasil splice
    console.log(detail);


    //nama bulan dengan split dan switch
    var bulan       = data[3].split("/");

    var bulan1 = [];
    for(var x = 0; x < bulan.length ; x++){
        bulan1[x] = Number(bulan[x]);
    }

    var namaBulan   = bulan1[1];
    
    switch(namaBulan){
        case 1 : {
            console.log("Januari");
            break;
        }
        case 2 : {
            console.log("Februari");
            break;
        }
        case 3 : {
            console.log("Maret");
            break;
        }
        case 4 : {
            console.log("April");
            break;
        }
        case 5 : {
            console.log("Mei");
            break;
        }
        case 6 : {
            console.log("Juni");
            break;
        }
        case 7 : {
            console.log("Juli");
            break;
        }
        case 8 : {
            console.log("Agustus");
            break;
        }
        case 9 : {
            console.log("September");
            break;
        }
        case 10 : {
            console.log("Oktober");
            break;
        }
        case 11 : {
            console.log("November");
            break;
        }
        case 12 : {
            console.log("Desember");
            break;
        }
    }


    //sorting bulan secara descending
    var sortingBulan    = bulan1.sort();
    var strSortBulan    = [];
    var y;

    for(y = 0; y < sortingBulan.length ; y++){
        strSortBulan[y] = String(sortingBulan[y]);
    }
    console.log(strSortBulan);


    //menggabungkan dengan pemisah tanda '-'
    var joinBulan   = bulan.join('-');
    console.log(joinBulan);


    //menghapus karakter max 15 karakter
    var arrNama             = String(data[1]);
    var hapusKarakterNama   = arrNama.slice(0,14);

    console.log(hapusKarakterNama);

}

console.log('-------- 6. METODE ARRAY -----------');

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

console.log('\n');
/*-----------------------------------------------------*/
