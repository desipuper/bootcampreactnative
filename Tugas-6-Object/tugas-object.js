 /*------------------------------ 1. Array to Object----------------------------------*/

// Buatlah function dengan nama arrayToObject() yang menerima sebuah parameter berupa array multidimensi. 
// Dalam array tersebut berisi value berupa First Name, Last Name, Gender, dan Birthyear. Data di dalam array
// dimensi tersebut ingin kita ubah ke dalam bentuk Object dengan key bernama : firstName, lastName, gender, 
// dan age. Untuk key age ambillah selisih tahun yang ditulis di data dengan tahun sekarang. Jika tahun tidak 
// terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai :
//  “Invalid birth year”.

// Contoh: jika input nya adalah [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]

//maka outputnya di console seperti berikut :
/*
1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
*/



console.log("------------- 1. Array to Object ----------------");

var now         = new Date();
var thisYear    = now.getFullYear();

//CODE

function arrayToObject(arr) {
         
    var dataDetail  = {};
    var data        = [];
    var name        = '';
    var no          = 0;


    for(var i = 0; i < arr.length ; i++) {
        if ( !arr[i][3] || arr[i][3] > thisYear){
            var ageHasil = "Invalid birth year";
        } else {
            var ageHasil = thisYear - arr[i][3];
        }

        no = i+1;
        dataDetail   = {
            firstName  : arr[i][0],
            lastName   : arr[i][1],
            gender     : arr[i][2],
            age        : ageHasil
        }

        name = no + '. ' + arr[i][0] + ' ' + arr[i][1];
        data[name] = dataDetail;
        
    }
    console.log(data);
}
 


// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log('\n');

/*----------------------------------------------------------------------------------- */







 /*------------------------------ 2. Shopping Time----------------------------------*/

 // Diberikan sebuah function shoppingTime(memberId, money) yang menerima dua parameter berupa string dan number.
 // Parameter pertama merupakan memberId dan parameter ke-2 merupakan value uang (money) yang dibawa oleh 
 // member tersebut.

 // Toko X sedang melakukan SALE untuk beberapa barang, yaitu:
 /*
 Sepatu brand Stacattu seharga 1500000
 Baju brand Zoro seharga 500000
 Baju brand H&N seharga 250000
 Sweater brand Uniklooh seharga 175000
 Casing Handphone seharga 50000
 */
 
 // Buatlah function yang akan mengembalikan sebuah object dimana object tersebut berisikan informasi 
 // memberId, money, listPurchased dan changeMoney.
 
 // Jika memberId kosong maka tampilkan “Mohon maaf, toko X hanya berlaku untuk member saja”
 // Jika uang yang dimiliki kurang dari 50000 maka tampilkan “Mohon maaf, uang tidak cukup”
 // Member yang berbelanja di toko X akan membeli barang yang paling mahal terlebih dahulu dan akan 
 // membeli barang-barang yang sedang SALE masing-masing 1 jika uang yang dimilikinya masih cukup.

 // Contoh jika inputan memberId: ‘324193hDew2’ dan money: 700000
 
 // maka output:
 /*
 { memberId: ‘324193hDew2’, money: 700000, listPurchased: [ ‘Baju Zoro’, ‘Sweater Uniklooh’ ], changeMoney: 25000 }
*/


console.log("------------- 2. Shopping Time ----------------");


function shoppingTime(memberId, money) {
    

    if(!memberId){
        var hasil = "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        if(money < 50000) {
            var hasil = "Mohon maaf, uang tidak cukup";
        } else {
            
            var changeMoney = money;
            var listPurchased = [];

            if (changeMoney >= 1500000){
                changeMoney     -= 1500000;
                listPurchased.push('Sepatu Stacattu');
            } 
            
            if (changeMoney >= 500000) {
                changeMoney -= 500000;
                listPurchased.push('Baju Zoro');
            }

            if (changeMoney >= 250000) {
                changeMoney -= 250000;
                listPurchased.push('Baju H&N');
            }

            if (changeMoney >= 175000) {
                changeMoney -= 175000;
                listPurchased.push('Sweater Uniklooh');
            }

            if (changeMoney >= 50000) {
                changeMoney -= 50000;
                listPurchased.push('Casing Handphone');
            }

            var hasil = {
                memberId    : memberId,
                money       : money,
                listPurchased: listPurchased,
                changeMoney : changeMoney
            }
        }
    }

    return hasil;

}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n');

/*----------------------------------------------------------------------------------- */








 /*------------------------------ 3. Naik Angkot----------------------------------*/
 // Diberikan function naikAngkot(listPenumpang) yang akan menerima satu parameter berupa array dua dimensi. 
 // Function akan me-return array of object.

 // Diberikan sebuah rute, dari A – F. Penumpang diwajibkan membayar Rp2000 setiap melewati satu rute.
 
 // Contoh: input: 
 /* 
 [ [‘Dimitri’, ‘B’, ‘F’] ] output: [{ penumpang: ‘Dimitri’, naikDari: ‘B’, tujuan: ‘F’, bayar: 8000 }]
 */
 
console.log("------------- 3. Naik Angkot ----------------");
 

//Code
 
 function naikAngkot(arrPenumpang) {

    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var step = 0;
    var hasil = [];
    var bayar = 0;
    var detail = {};

    for(var i = 0; i < arrPenumpang.length; i++){

        index1 = rute.indexOf(arrPenumpang[i][1]);
        index2 = rute.indexOf(arrPenumpang[i][2]);

        if(index1 > index2){
            step    = index1 - index2;
        } else {
            step    = index2 - index1;
        }

        bayar = step * 2000;

        detail = {
            penumpang   : arrPenumpang[i][0],
            naikDari    : arrPenumpang[i][1],
            tujuan      : arrPenumpang[i][2],
            bayar       : bayar
        }

        hasil[i] = detail; 
        
    }

    return hasil;
 }
  
 //TEST CASE
 console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B'], ['Bayu', 'F', 'C']]));
 // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
 //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  
 console.log(naikAngkot([])); //[]

console.log('\n');

/*----------------------------------------------------------------------------------- */