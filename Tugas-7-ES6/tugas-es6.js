/*----------------------- 1. Mengubah fungsi menjadi fungsi arrow -------------------------*/

// Code ES5
/*
const golden = function goldenFunction(){
    console.log("this is golden!!")
}
*/

// Code ES6
const golden = () => {
    console.log("this is golden!!");
}

console.log('--------- 1. Fungsi Arrow ---------');
golden();
console.log('\n');


/*------------------------------------------------------------------------------*/





/*----------------------- 2. Sederhanakan menjadi object literal di ES6 -------------------------*/

// ES5
/*
const newFunction = function literal(firstName, lastName){
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
          console.log(firstName + " " + lastName)
          return 
        }
      }
    }
*/

// ES6
const newFunction = (firstName, lastName) => {
    return{
        firstName , 
        lastName, 
        fullName() {
            return console.log(`${firstName} ${lastName}`);
        }
    }
}


console.log('--------- 2. Object Literal---------');

//Driver Code 
newFunction("William", "Imoh").fullName();

console.log('\n');


/*------------------------------------------------------------------------------*/





/*----------------------- 3. Destructuring -------------------------*/

// Diberikan sebuah objek sebagai berikut:
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// ES5
/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
*/


//ES6
const {firstName, lastName, destination, occupation} = newObject;

console.log('--------- 3. Destructuring ---------');

// Driver code
console.log(firstName, lastName, destination, occupation);

console.log('\n');


/*------------------------------------------------------------------------------*/





/*----------------------- 4. Array Spreading -------------------------*/

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]


// ES5
/*
const combined = west.concat(east)
*/


//ES6
const combined = [...west, ...east];

console.log('--------- 4. Array Spreading ---------');

//Driver Code
console.log(combined);

console.log('\n');


/*------------------------------------------------------------------------------*/





/*----------------------- 5. Template Literals -------------------------*/

const planet = "earth"
const view = "glass"

//ES5
/*
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit, ' + planet + ' do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 */

//ES6
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
            `incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;


console.log('--------- 5. Template Literals ---------');

// Driver Code
console.log(before);

console.log('\n');


/*------------------------------------------------------------------------------*/

