/*------------------------- Call Back Baca Buku -----------------------------*/

var readBooks   = require('./callback.js');

var books   = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]


//code untuk memanggil function readBooks 
var times    = 10000;
var x        = 0;

function panggilReadBooks(time, books, i){

    readBooks(time, books[i], function(sisaWaktu){
        i++;
        if(i < books.length && sisaWaktu > 0 ){
            if(sisaWaktu > books[i].timeSpent){
                panggilReadBooks(sisaWaktu, books, i);
            }
        } else {
            i = 0;
            panggilReadBooks(sisaWaktu, books, i);
        }
    });

}

panggilReadBooks(times, books, x);