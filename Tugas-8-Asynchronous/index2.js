/*--------------------- Promise Baca Buku --------------------*/

var readBooksPromise    = require('./promise.js');

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// code untuk menjalankan function readBooksPromise

var times    = 10000;
var x        = 0;

function jalankanReadBooksPromise(time, books, i){

    readBooksPromise(time, books[i])
        .then(function (fulfilled) {
            i++;
            if(i < books.length && fulfilled > 0 ){
                if(fulfilled > books[i].timeSpent){
                    jalankanReadBooksPromise(fulfilled, books, i);
                }
            } else {
                i = 0;
                jalankanReadBooksPromise(fulfilled, books, i);
            }
        })

        .catch(function (error) {
            if(error > 0) {
                jalankanReadBooksPromise(error, books, i);
            }
        })
}


jalankanReadBooksPromise(times, books, x);